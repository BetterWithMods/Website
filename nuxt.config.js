import VuetifyLoaderPlugin from 'vuetify-loader/lib/plugin'
import pkg from './package'
import path from 'path'

const md = require("markdown-it")({
  html: true
});


const {CI_PAGES_URL} = process.env;
const base = CI_PAGES_URL && new URL(CI_PAGES_URL).pathname

export default {
  mode: 'spa',

  /*
   ** Headers of the page
   */
  head: {
    title: 'Better With Mods',
    meta: [
      {charset: 'utf-8'},
      {name: 'viewport', content: 'width=device-width, initial-scale=1'},
      {hid: 'description', name: 'description', content: pkg.description}
    ],
    link: [
      {rel: 'icon', type: 'image/x-icon', href: '/favicon.ico'},
      {
        rel: 'stylesheet',
        href:
          'https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Material+Icons'
      }
    ]
  },

  /*
   ** Customize the progress-bar color
   */
  loading: {color: '#fff'},

  /*
   ** Global CSS
   */
  css: ['~/assets/style/app.styl'],


  generate: {
    dir: 'public'
  },


  router: {
    base
  },


  /*
   ** Nuxt.js modules
   */
  modules: [
    '@nuxtjs/vuetify'
  ],

  vuetify: {
    // iconfont: 'fa'
  },

  plugins: [
    '@/plugins/vuetify'
  ],

  /*
   ** Build configuration
   */
  build: {
    transpile: ['vuetify/lib'],
    plugins: [new VuetifyLoaderPlugin()],
    loaders: {
      stylus: {
        import: ['~assets/style/variables.styl']
      }
    },
    /*
     ** You can extend webpack config here
     */
    extend(config, ctx) {
      // Run ESLint on save
      if (ctx.isDev && ctx.isClient) {
        config.module.rules.push(
          {
            enforce: 'pre',
            test: /\.(js|vue)$/,
            loader: 'eslint-loader',
            exclude: /(node_modules)/
          }
        )
      }

      config.module.rules.push({
        test: /\.md$/,
        loader: 'frontmatter-markdown-loader',
        include: path.resolve(__dirname, 'content'),
        options: {
          vue: true,
          markdown: (body) => {
            return md.render(body)
          }
        }
      })
    }
  }
}
