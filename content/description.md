### The Better With Mods Suite


The Better With Mods Suite is a collection of mods based around extending and enhancing the experience of the Minecraft world. Focusing on improving the game with tools of emergent behavior, additional content and lore.
In the process of this, all of the features are made highly accessible and configurable for players and modpack-makers alike change and manipulate the mods to their choosing.     


The suite contains the following mods—for more in-depth descriptions see the tabs above:


* Core  
* Arcanium  
* Mechanist  
* Phenomenons  
* Vitality  
<br/><br/>


**Installation Instructions:**

* Install using Twitch Launcher
* or download the jar from the Curseforge and add the `mods/` folder in your minecraft forge instance.






  


