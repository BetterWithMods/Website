# Frequently Asked Questions
  
There are some questions that this group gets very often, this will be the authoritative source for those answers.

* What is the relationship of this mod with Better Than Wolves?

  This is a complicated question without a definitive answer. The best way to explain it is by going over the history of Better With Mods.
  
  Better With Mods started as a pet project by BeetoGuy, who wanted the mechanical power features from Better Than Wolves in modern Minecraft.
  It was a simple port of a select few features that Beeto initially released for 1.7.10 and updated to 1.8 and 1.10 eventually.
  
  I, primetoxinz, first saw BWM in Better Than Minecon 2016, it was not yet on CurseForge or publicly available but I kept searching for it to help out.
  Eventually, the mod made it on the CurseForge and I promptly contacted BeetoGuy that I would like to start adding Minetweaker support to the machines that were in the mod at the time.
  In doing this, I managed to get Beeto to open-source BWM and put the code on Github. This was mainly to help us collaborate on what already existed in the mod.
  
  By open-sourcing the mod, it slowly evolved as people that were familiar with Better Than Wolves started to make suggestions of features to port from the original.
  I can't speak for Beeto on this, but I don't think the intention was ever to make a full clone, just to add back some of the cooler features from a creation he enjoyed.
  
  As time progressed, I got full rights to the repository and mostly took over the codebase of Better With Mods and with time the feature set continued to grow and grow.
  More of the hardcore concepts, items and blocks from BTW made it into BWM. Around the 1.10 to 1.11 port, I decided to make the mod more modular, having all the game tweaks and hardcore changes be entirely modular,
  most parts could be disabled or changed to be fit the player. We continued to gather more popularity, players from the old BTW community showed up as well as players that had never even heard of the original.
  
  Once we finally ported up to 1.12, we were a pretty full clone of BTW, minus some more difficult features to implement in a Minecraft Forge environment.
  This internally troubled me a bit,I would have liked some individuality, but I didn't let that get me down. 
  While it was not bad, I don't really want to go into the details of what happened next. We internally came into a string of middle-man communications with Flowerchild, the original creator of Better Than Wolves.
  Previously, he seems to have known that BWM existed but hadn't put much thought into it, but now that there was communications he made it well-known he was thoroughly pissed that this mod existed. That was really all that came of it.
  
  I did get banned from the BTW forums, though.
  
  To really answer the question, this mod takes deep inspiration from a creation that many of us enjoy and think is very well made.
  We want to move away from the identity of a clone but with how similar most of the content is that is unlikely to happen in full.
  
  In reality, we are different enough in philosophy, one of openness and being friendly to those who want to play games differently than others. 
  
  
  
  
  
        
  
  
   
  
   
 

 

