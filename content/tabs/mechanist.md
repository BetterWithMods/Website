---
title: Mechanist
badge: 
---

Mechanist focuses on the expansion of the tech tree:  

* Additional redstone devices, such as the Advanced Dispenser, Detector and the Hibachi.    
* The introduction of mechanical power, a system for harnessing the rotation of windmills or waterwheels to power new machines through gearboxes and axles.  
* Focus on automation with utility blocks that make mob traps and fully automatic crop farms closer to the original style of Minecraft.



